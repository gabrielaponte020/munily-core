const Firebase = use('Adonis/Services/Firebase');

class UserController {
  async getById({ params }) {
    try {
      const snapUser = await Firebase.db().collection('user').doc(params.id).get();

      if (snapUser.exists) {
        const data = snapUser.data();
        return data;
      }
      return null;
    } catch (err) {
      console.log('user.firebase - getById');
      console.log(err);
      throw new Error('FR - Error buscando a usuario por id');
    }
  }

  async protected() {
    return 'protected';
  }
}

module.exports = UserController;
