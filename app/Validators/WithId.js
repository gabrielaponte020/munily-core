

class WithId {
  get rules() {
    return {
      id: 'required',
    };
  }
}

module.exports = WithId;
