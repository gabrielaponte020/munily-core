
const Firebase = use('Adonis/Services/Firebase');

class FirebaseAuth {
  /**
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Function} next
   */
  async handle({ request, response }, next) {
    const authorization = request.header('Authorization');
    if (authorization) {
      try {
        await Firebase.auth().verifyIdToken(authorization);
        await next();
      } catch (e) {
        response.status(401).send('No authenticated');
      }
    } else response.status(401).send('Authorization header is not found');
  }
}

module.exports = FirebaseAuth;
