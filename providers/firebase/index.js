const firebaseAdmin = require('firebase-admin');

class Index {
  constructor(Config) {
    // Obtain Firebase configuration from config/services.js
    this.Config = Config.get('services.firebase');

    // Initialize our priviledged firebase admin application
    firebaseAdmin.initializeApp({
      credential: firebaseAdmin.credential.cert(this.Config.credentials),
      databaseURL: this.Config.databaseURL,
      storageBucket: this.Config.bucketName,
    });
  }

  admin() {
    return firebaseAdmin;
  }

  auth() {
    return firebaseAdmin.auth();
  }

  db() {
    return firebaseAdmin.firestore();
  }

  bucket() {
    firebaseAdmin.storage().bucket(this.Config.bucketName);
  }
}

module.exports = Index;
