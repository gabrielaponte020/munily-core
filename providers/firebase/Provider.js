

const { ServiceProvider } = require('@adonisjs/fold');
const FirebaseService = require('./');

class Provider extends ServiceProvider {
  /**
   * Register namespaces to the IoC container
   *
   * @method register
   *
   * @return {void}
   */
  register() {
    this.app.bind('Adonis/Services/Firebase', (app) => {
      // Obtain application configuration in config/
      const Config = app.use('Adonis/Src/Config');

      // Export our service
      return new FirebaseService(Config);
    });
  }

  /**
   * Attach context getter when all providers have
   * been registered
   *
   * @method boot
   *
   * @return {void}
   */
  boot() {
    //
  }
}

module.exports = Provider;
